package ru.itis.fivecg.dto;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import ru.itis.fivecg.database.domain.UserDetails;

import javax.validation.constraints.NotNull;
import java.util.List;

public class UserInfo
{
    private long id;
    private String email;
    private String phone;
    private String netStatus;
    @NotNull
    @Length(min = 2, max = 40)
    private String firstName;
    @NotNull
    @Length(min = 2, max = 40)
    private String lastName;
    private UserDetails.Gender gender;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private String birthDate;
    @Length(min = 2, max = 100)
    private String country;
    @Length(min = 2, max = 100)
    private String city;
    private String photoDate;

    private boolean showUser;
    private boolean showLocation;
    private boolean showCars;

    private List<UserCarDTO> cars;

    public UserInfo() { }
    public UserInfo(long id, String email, String phone, String netStatus, String firstName,
                    String lastName, UserDetails.Gender gender, String birthDate, String country, String city,
                    String photoDate, boolean showUser, boolean showLocation, boolean showCars,
                    List<UserCarDTO> cars)
    {
        this.id = id;
        this.email = email;
        this.phone = phone;
        this.netStatus = netStatus;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthDate = birthDate;
        this.country = country;
        this.city = city;
        this.photoDate = photoDate;
        this.showUser = showUser;
        this.showLocation = showLocation;
        this.showCars = showCars;
        this.cars = cars;
    }

    public long getId()
    {
        return id;
    }

    public String getEmail()
    {
        return email;
    }

    public String getPhone()
    {
        return phone;
    }

    public String getNetStatus()
    {
        return netStatus;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public UserDetails.Gender getGender()
    {
        return gender;
    }

    public String getBirthDate()
    {
        return birthDate;
    }

    public String getCountry()
    {
        return country;
    }

    public String getCity()
    {
        return city;
    }

    public String getPhotoDate()
    {
        return photoDate;
    }

    public boolean isShowUser()
    {
        return showUser;
    }

    public boolean isShowLocation()
    {
        return showLocation;
    }

    public boolean isShowCars()
    {
        return showCars;
    }

    public List<UserCarDTO> getCars()
    {
        return cars;
    }
}
