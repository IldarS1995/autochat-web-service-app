package ru.itis.fivecg.exception;

/**
 * Исключение, которое выбрасывается, если какое-то из полей электронного сообщения
 * является нулевым
 */
public class EmailMalformedException extends RuntimeException
{
    public EmailMalformedException() { }
    public EmailMalformedException(String message)
    {
        super(message);
    }
}
