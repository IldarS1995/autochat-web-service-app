package ru.itis.fivecg.service.impl.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.fivecg.database.dao.AppUserRepository;
import ru.itis.fivecg.database.dao.CarRepository;
import ru.itis.fivecg.database.dao.UserAccountDetailsRepository;
import ru.itis.fivecg.database.dao.UserDetailsRepository;
import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.database.domain.Car;
import ru.itis.fivecg.database.domain.UserAccountDetails;
import ru.itis.fivecg.database.domain.UserDetails;
import ru.itis.fivecg.dto.UserDetailsDTO;
import ru.itis.fivecg.dto.UserInfo;
import ru.itis.fivecg.exception.UserIsNullException;
import ru.itis.fivecg.exception.UserNotFoundException;
import ru.itis.fivecg.service.AppUserDetailsService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static ru.itis.fivecg.dto.DtoUtil.convertToDto;

@Service
@Transactional
public class AppUserDetailsServiceImpl implements AppUserDetailsService
{
    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private UserDetailsRepository userDetailsRepository;
    @Autowired
    private UserAccountDetailsRepository userAccountDetailsRepository;
    @Autowired
    private CarRepository carRepository;

    private SimpleDateFormat dateFmt = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    public UserDetailsDTO getUserDetails(long id)
    {
        return convertToDto(userDetailsRepository.findByUser_Id(id));
    }

    @Override
    public void addDetails(UserDetails ud)
    {
        if(ud == null)
        {
            throw new IllegalArgumentException("Argument user details must not be null.");
        }
        if(ud.getUser() == null)
        {
            throw new UserIsNullException();
        }

        //TODO:DELETE THIS LINE:
        ud.setId(ud.getUser().getId());
        userDetailsRepository.save(ud);
    }

    @Override
    public void addUserAccountDetails(AppUser user)
    {
        if(user == null)
        {
            throw new IllegalArgumentException("User argument can't be null.");
        }

        UserAccountDetails accountDetails = new UserAccountDetails(user);
        userAccountDetailsRepository.save(accountDetails);
    }

    @Override
    public UserInfo getFullUserInformation(long userId)
    {
        AppUser user = appUserRepository.findOne(userId);
        if(user == null)
        {
            throw new UserNotFoundException();
        }

        UserDetails userDetails = userDetailsRepository.findByUser_Id(userId);
        UserAccountDetails userAccountDetails = userAccountDetailsRepository.findByUser_Id(userId);
        List<Car> userCars = carRepository.findByOwner_Id(userId);
        return convertToDto(userDetails, userAccountDetails, userCars);
    }

    @Override
    public void updateUserInfo(String email, UserInfo userInfo)
    {
        AppUser user = appUserRepository.findByEmail(email);
        user.setFirstName(userInfo.getFirstName());
        user.setLastName(userInfo.getLastName());

        UserDetails userDetails = userDetailsRepository.findByUser_Id(user.getId());
        try
        {
            userDetails.setBirthDate(dateFmt.parse(userInfo.getBirthDate()));
        }
        catch (ParseException e)
        {
            //Не должно произойти
        }
        userDetails.setCity(userInfo.getCity());
        userDetails.setCountry(userInfo.getCountry());
        userDetails.setGender(userInfo.getGender());

        UserAccountDetails accountDetails = userAccountDetailsRepository.findByUser_Id(user.getId());
        accountDetails.setShowCars(userInfo.isShowCars());
        accountDetails.setShowLocation(userInfo.isShowLocation());
        accountDetails.setShowUser(userInfo.isShowUser());
    }

    @Override
    public void updateNetStatus(String email, String status)
    {
        UserDetails userDetails = userDetailsRepository.findByUser_Email(email);
        userDetails.setNetStatus(UserDetails.USER_STATUS.valueOf(status));
    }
}
