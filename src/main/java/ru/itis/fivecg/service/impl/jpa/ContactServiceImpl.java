package ru.itis.fivecg.service.impl.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.fivecg.database.dao.AppUserRepository;
import ru.itis.fivecg.database.dao.ContactRepository;
import ru.itis.fivecg.database.domain.Contact;
import ru.itis.fivecg.dto.UserContactDTO;
import ru.itis.fivecg.service.ContactService;

import java.util.ArrayList;
import java.util.List;

import static ru.itis.fivecg.dto.DtoUtil.convertToDto;

@Service
@Transactional
public class ContactServiceImpl implements ContactService
{
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private AppUserRepository appUserRepository;

    @Override
    public List<UserContactDTO> getContactsByOwnerEmail(String email)
    {
        List<Contact> contacts = contactRepository.findByContactOwner_Email(email);
        List<UserContactDTO> result = new ArrayList<>();
        for(Contact contact : contacts)
        {
            result.add(convertToDto(contact));
        }
        return result;
    }

    @Override
    public Contact getContact(String email, Long userId)
    {
        return contactRepository.findByContactOwner_EmailAndAnotherUser_Id(email, userId);
    }

    @Override
    public void addContact(String email, Long userId)
    {
        Contact contact = new Contact();
        contact.setContactOwner(appUserRepository.findByEmail(email));
        contact.setContactStatus(Contact.ContactStatus.FRIEND);
        contact.setAnotherUser(appUserRepository.findOne(userId));
        contactRepository.save(contact);
    }

    @Override
    public void deleteContact(Contact contact)
    {
        contactRepository.delete(contact);
    }
}
