package ru.itis.fivecg.dto;

public class FieldMessage
{
    private String field;
    private String message;
    private String statusCode;

    public FieldMessage() { }
    public FieldMessage(String field, String message, String statusCode)
    {
        this.field = field;
        this.message = message;
        this.statusCode = statusCode;
    }

    public String getField()
    {
        return field;
    }

    public String getMessage()
    {
        return message;
    }

    public String getStatusCode()
    {
        return statusCode;
    }
}
