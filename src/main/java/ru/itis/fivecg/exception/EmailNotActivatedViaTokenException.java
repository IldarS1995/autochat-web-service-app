package ru.itis.fivecg.exception;

/**
 * Исключение, которое выбрасывается, когда юзер пытается активировать E-Mail
 * через аутентификацию, но перед этим не передал секретный ключ на сервер
 */
public class EmailNotActivatedViaTokenException extends Exception
{
    public EmailNotActivatedViaTokenException()
    {
    }
    public EmailNotActivatedViaTokenException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
