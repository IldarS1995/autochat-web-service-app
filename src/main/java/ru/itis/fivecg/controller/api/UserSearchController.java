package ru.itis.fivecg.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.fivecg.dto.FieldMessage;
import ru.itis.fivecg.dto.InfoDTO;
import ru.itis.fivecg.dto.SearchInfoDTO;
import ru.itis.fivecg.dto.UserInfo;
import ru.itis.fivecg.service.AppUserService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/users/search")
public class UserSearchController
{
    @Autowired
    private AppUserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<InfoDTO<List<SearchInfoDTO>>> searchUsers(@RequestBody SearchInfoDTO searchInfo)
    {
        boolean success = true;
        List<FieldMessage> messages = new ArrayList<>();

        if(empty(searchInfo.getFirstName()) && !empty(searchInfo.getLastName())
                || empty(searchInfo.getLastName()) && !empty(searchInfo.getFirstName()))
        {
            success = false;
            messages.add(new FieldMessage("firstName",
                    "firstName and lastName must be used together.", "REQ_FIELD_NOT_SPECIFIED"));
        }

        List<SearchInfoDTO> users = null;
        if(messages.size() == 0)
        {
            try
            {
                users = userService.searchUsers(searchInfo);
            }
            catch(IllegalArgumentException exc)
                    //Никакие поля для поиска не указаны
            {
                success = false;
                messages.add(new FieldMessage("fields",
                        "At least one field must be specified for searching.",
                        "FIELDS_NOT_SPECIFIED"));
            }
        }

        return new ResponseEntity<>(
            new InfoDTO<>(success, messages, users),
            success ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    private boolean empty(String str)
    {
        return str == null || str.trim().length() == 0;
    }
}
