package ru.itis.fivecg.dto;

import ru.itis.fivecg.database.domain.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public final class DtoUtil
{
    private static SimpleDateFormat dateFmt = new SimpleDateFormat("dd/MM/yyyy");
    private static SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy hh:mm");

    private DtoUtil() { }

    public static UserContactDTO convertToDto(Contact contact)
    {
        AppUser user = contact.getAnotherUser();
        UserDetails ud = user.getUserDetails();
        String photoDate = ud.getPhotoDate() != null ? fmt.format(ud.getPhotoDate()) : null;
        return new UserContactDTO(
                user.getId(), user.getCellPhone(), user.getEmail(),
                user.getFirstName() + " " + user.getLastName(), photoDate,
                ud.getNetStatus().toString(), contact.getContactStatus().toString());
    }

    public static UserDetailsDTO convertToDto(UserDetails ud)
    {
        if(ud == null)
            return null;

        String birthStr = ud.getBirthDate() != null ? fmt.format(ud.getBirthDate()) : "null";

        return new UserDetailsDTO(ud.getUser().getId(), ud.getGender().name(),
                birthStr, ud.getCountry(), ud.getCity(), ud.getNetStatus().name());
    }

    public static SearchInfoDTO convertToDto(AppUser user, String identification)
    {
        return new SearchInfoDTO(String.valueOf(user.getId()), user.getFirstName(), user.getLastName(),
                user.getEmail(), user.getCellPhone(), identification, null);
    }

    public static UserAuthDataDTO convertToDto(AppUser u)
    {
        if(u == null)
            return null;

        return new UserAuthDataDTO(u.getId(), u.getEmail(), u.getFirstName(), u.getLastName(),
                u.getCellPhone(), u.getPassword(), null, null, null);
    }

    public static UserCarDTO convertToDto(Car car)
    {
        if(car == null)
            return null;

        CarPhoto carPhoto = car.getPhoto();
        String createdAt = carPhoto.getCreatedAt() != null ?
                dateFmt.format(carPhoto.getCreatedAt()) : null;
        CarPhotoDTO carPhotoDTO = new CarPhotoDTO(carPhoto.getPhotoId(), createdAt);

        return new UserCarDTO(car.getBrand(), car.getModel(), car.getIdentification(),
                car.getColor(), carPhotoDTO);
    }

    public static AppUser convertFromDto(UserAuthDataDTO user)
    {
        AppUser ret = new AppUser();
        ret.setEmail(user.getEmail());
        ret.setFirstName(user.getFirstName());
        ret.setLastName(user.getLastName());
        ret.setPassword(user.getPassword());
        ret.setCellPhone(user.getPhone());
        return ret;
    }

    public static Car convertFromDto(AppUser owner, UserCarDTO car)
    {
        Car ret = new Car();
        ret.setOwner(owner);
        ret.setBrand(car.getBrand());
        ret.setColor(car.getColor());
        ret.setIdentification(car.getIdentification());
        ret.setModel(car.getModel());
        ret.setDefaultCar(car.isDefaultCar());
        return ret;
    }


    public static UserInfo convertToDto(UserDetails userDetails,
                                           UserAccountDetails userAccountDetails,
                                        List<Car> userCars)
    {
        AppUser user = userDetails.getUser();
        String birthDate = userDetails.getBirthDate() == null ? null :
                dateFmt.format(userDetails.getBirthDate());
        String photoDate = userDetails.getPhotoDate() == null ? null :
                fmt.format(userDetails.getPhotoDate());
        String netStatus = userDetails.getNetStatus() == null ? null :
                userDetails.getNetStatus().name();

        List<UserCarDTO> cars = new ArrayList<>();
        for(Car car: userCars)
        {
            cars.add(convertToDto(car));
        }

        return new UserInfo(
                user.getId(), user.getEmail(), user.getCellPhone(), netStatus,
                user.getFirstName(), user.getLastName(), userDetails.getGender(), birthDate,
                userDetails.getCountry(), userDetails.getCity(), photoDate,
                userAccountDetails.isShowUser(), userAccountDetails.isShowLocation(),
                userAccountDetails.isShowCars(), cars
        );
    }

    public static MessageCreationResultDTO convertToDto(Message message)
    {
        String createdAt = fmt.format(message.getCreatedAt());
        return new MessageCreationResultDTO(message.getId(), createdAt);
    }
}
