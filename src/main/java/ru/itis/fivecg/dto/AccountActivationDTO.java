package ru.itis.fivecg.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class AccountActivationDTO
{
    @NotNull
    @Length(min = 3, max = 50)
    private String emailOrPhone;
    @NotNull
    @Length(min = 5, max = 100)
    private String checkCode;

    public AccountActivationDTO() { }
    public AccountActivationDTO(String emailOrPhone, String checkCode)
    {
        this.emailOrPhone = emailOrPhone;
        this.checkCode = checkCode;
    }

    public String getEmailOrPhone()
    {
        return emailOrPhone;
    }

    public String getCheckCode()
    {
        return checkCode;
    }
}
