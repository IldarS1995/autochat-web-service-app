package ru.itis.fivecg.exception;

public class UserIsNullException extends RuntimeException
{
    public UserIsNullException() { }
    public UserIsNullException(String msg) { super(msg); }
}
