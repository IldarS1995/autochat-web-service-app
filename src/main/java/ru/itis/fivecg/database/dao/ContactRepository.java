package ru.itis.fivecg.database.dao;

import org.springframework.data.repository.CrudRepository;
import ru.itis.fivecg.database.domain.Contact;

import java.util.List;

public interface ContactRepository extends CrudRepository<Contact, Long>
{
    List<Contact> findByContactOwner_Email(String email);

    Contact findByContactOwner_EmailAndAnotherUser_Id(String email, Long userId);
}
