package ru.itis.fivecg.database.domain;

import javax.persistence.*;

@Entity
@Table(name = "app_user")
public class AppUser
{
    @Id
    @Column(name = "user_id")
    @GeneratedValue
    private Long id;

    @Column(name = "first_name", length = 40, nullable = false)
    private String firstName;

    @Column(name = "last_name", length = 40, nullable = false)
    private String lastName;

    @Column(unique = true, nullable = false, length = 50)
    private String email;

    @Column(name = "cell_phone", nullable = false, unique = true, length = 20)
    private String cellPhone;

    @Column(nullable = false, length = 50)
    private String password;

    @Column(name = "current_latitude")
    private Double currentLatitude;

    @Column(name = "current_longitude")
    private Double currentLongitude;

    @OneToOne(mappedBy = "user", fetch = FetchType.LAZY)
    private UserDetails userDetails;

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppUser appUser = (AppUser) o;

        if (!email.equals(appUser.email)) return false;
        if (!cellPhone.equals(appUser.cellPhone)) return false;
        return password.equals(appUser.password);

    }

    @Override
    public int hashCode()
    {
        int result = email.hashCode();
        result = 31 * result + cellPhone.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getCellPhone()
    {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone)
    {
        this.cellPhone = cellPhone;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public Double getCurrentLatitude()
    {
        return currentLatitude;
    }

    public void setCurrentLatitude(Double currentLatitude)
    {
        this.currentLatitude = currentLatitude;
    }

    public Double getCurrentLongitude()
    {
        return currentLongitude;
    }

    public void setCurrentLongitude(Double currentLongitude)
    {
        this.currentLongitude = currentLongitude;
    }

    public UserDetails getUserDetails()
    {
        return userDetails;
    }
}
