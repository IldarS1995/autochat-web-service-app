package ru.itis.fivecg.database.dao;

import org.springframework.data.repository.CrudRepository;
import ru.itis.fivecg.database.domain.CarPhoto;

public interface CarPhotoRepository extends CrudRepository<CarPhoto, Long>
{
    CarPhoto findByCar_CarId(long carId);
}
