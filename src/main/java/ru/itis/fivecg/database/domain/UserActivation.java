package ru.itis.fivecg.database.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Сущность "Активация E-Mail пользователя"
 */
@Entity
@Table(name = "user_activation")
public class UserActivation
{
    /** Первичный ключ сущности */
    @Id
    @Column(name = "activation_id")
    @GeneratedValue
    private Long activationId;

    /** Пользователь, чей E-Mail подлежит активации */
    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false, unique = true)
    private AppUser user;

    /** Секретный ключ, который должен передать на сервер юзер для подтверждения своего E-Mail */
    @Column(nullable = false, length = 100, unique = true)
    private String token;

    /** Дата генерации ключа активации(а также этой сущности) */
    @Column(name = "generation_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date generationDate;

    /** Дата активации E-Mail */
    @Column(name = "activation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date activationDate;

    /**
     * Указывает на то, была ли произведена юзером активация через передачу секретного ключа на сервер
     * (т.е. через нажатие ссылки в пришедшем почтовом сообщении).
     * Это - первая фаза активации E-Mail. */
    @Column(name = "activated_via_token", nullable = false)
    private Boolean activatedViaToken;
    /**
     * Указывает на то, была ли произведена юзером полная активация E-Mail путем входа в систему
     * непосредственно после нажатия ссылки с ключом в почтовом сообщении.
     * Это - вторая и завершающая фаза активации E-Mail. */
    @Column(name = "activated_via_login", nullable = false)
    private Boolean activatedViaLogin;

    public UserActivation()
    {
        generationDate = new Date();
        activatedViaToken = false;
        activatedViaLogin = false;
    }

    public Long getActivationId()
    {
        return activationId;
    }

    public void setActivationId(Long activationId)
    {
        this.activationId = activationId;
    }

    public AppUser getUser()
    {
        return user;
    }

    public void setUser(AppUser user)
    {
        this.user = user;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public Date getGenerationDate()
    {
        return generationDate;
    }

    public void setGenerationDate(Date generationDate)
    {
        this.generationDate = generationDate;
    }

    public Date getActivationDate()
    {
        return activationDate;
    }

    public void setActivationDate(Date activationDate)
    {
        this.activationDate = activationDate;
    }

    public Boolean getActivatedViaToken()
    {
        return activatedViaToken;
    }

    public void setActivatedViaToken(Boolean activatedViaToken)
    {
        this.activatedViaToken = activatedViaToken;
    }

    public Boolean getActivatedViaLogin()
    {
        return activatedViaLogin;
    }

    public void setActivatedViaLogin(Boolean activatedViaLogin)
    {
        this.activatedViaLogin = activatedViaLogin;
    }
}
