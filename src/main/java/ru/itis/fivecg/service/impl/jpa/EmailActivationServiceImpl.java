package ru.itis.fivecg.service.impl.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.fivecg.database.dao.UserActivationRepository;
import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.database.domain.UserActivation;
import ru.itis.fivecg.exception.EmailNotActivatedViaTokenException;
import ru.itis.fivecg.exception.TokensNotMatchException;
import ru.itis.fivecg.service.EmailActivationService;
import ru.itis.fivecg.service.EmailService;

import javax.mail.MessagingException;
import java.util.Date;
import java.util.Random;

@Service
@Transactional
public class EmailActivationServiceImpl implements EmailActivationService
{
    @Autowired
    private UserActivationRepository userActivationRepository;
    @Autowired
    private EmailService emailService;

    private static final int ACTIVATION_NUMBER_LENGTH = 5;

    @Override
    public UserActivation getUserActivation(long userId)
    {
        return userActivationRepository.findByUserId(userId);
    }

    @Override
    public void activateEmailOfUser(long userId) throws EmailNotActivatedViaTokenException
    {

        UserActivation userActivation = userActivationRepository.findByUserId(userId);
        if(!userActivation.getActivatedViaToken())
            /* Если юзер не подтвердил до этого свой E-Mail через передачу секретного ключа
             * на сервер */
        {
            throw new EmailNotActivatedViaTokenException();
        }

        if(userActivation.getActivatedViaLogin())
            /* Если до этого этот E-Mail уже был полностью подтвержден */
        {
            return;
        }

        userActivation.setActivatedViaLogin(true);  /* Теперь юзер полностью подтвердил свой E-Mail */
        userActivation.setActivationDate(new Date());
    }

    @Override
    public void checkActivationToken(String email, String token) throws TokensNotMatchException
    {
        if(email == null || token == null)
        {
            throw new IllegalArgumentException("Поле email и/или token равен null.");
        }

        UserActivation activation = userActivationRepository.findByUser_Email(email);
        if (!activation.getToken().equals(token))
            /* Если переданный и подлинный ключи не совпадают */
        {
            throw new TokensNotMatchException();
        }

        activation.setActivatedViaToken(true);
    }

    @Override
    public UserActivation getUserActivationByEmail(String email)
    {
        if(email == null)
        {
            throw new IllegalArgumentException();
        }

        return userActivationRepository.findByUser_Email(email);
    }

    @Override
    public void deleteUserActivationOfUser(AppUser user)
    {
        if(user == null || user.getId() == null)
        {
            throw new IllegalArgumentException("Объект null и его поле id не должны быть равны null.");
        }

        userActivationRepository.delete(userActivationRepository.findByUserId(user.getId()));
        userActivationRepository.flush();
    }

    @Override
    public void generateTokenAndSendVerificationEmailToUser(AppUser user) throws MessagingException
    {
        if(user == null || user.getEmail() == null)
        {
            throw new IllegalArgumentException("Объект User и его поле E-Mail не должны быть равны null.");
        }

        deleteUserActivationOfUser(user);
        UserActivation userActivation = addUserActivationForUser(user);
        emailService.sendVerificationEmail(user.getEmail(), userActivation.getToken());
    }

    @Override
    public UserActivation addUserActivationForUser(AppUser user)
    {
        if(user == null || user.getId() == null)
        {
            throw new IllegalArgumentException("Объект user и поле Id не должны быть равны null.");
        }

        /* Создаем секретный ключ, который будет передан пользователю для подтверждения E-Mail */
        String randNumber = generateNumber(ACTIVATION_NUMBER_LENGTH);

        /* Создаем объект UserActivation для возможности подтверждения юзером своего E-Mail */
        UserActivation userActivation = new UserActivation();
        userActivation.setUser(user);
        userActivation.setToken(randNumber);
        return userActivationRepository.save(userActivation);
    }

    private String generateNumber(int n)
    {
        StringBuilder sb = new StringBuilder();
        Random rand = new Random(new Date().getTime());
        for(int i = 0;i < n;i++)
        {
            sb.append(rand.nextInt(10));
        }

        return sb.toString();
    }
}
