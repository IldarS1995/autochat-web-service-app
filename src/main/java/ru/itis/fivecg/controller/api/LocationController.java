package ru.itis.fivecg.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.fivecg.controller.ControllerUtils;
import ru.itis.fivecg.dto.FieldMessage;
import ru.itis.fivecg.dto.LocationDTO;
import ru.itis.fivecg.dto.QueryStatus;
import ru.itis.fivecg.service.AppUserService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/user/location")
public class LocationController
{
    @Autowired
    private AppUserService appUserService;

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public ResponseEntity<QueryStatus> updateGpsLocation(@RequestBody @Valid LocationDTO locationDTO,
                                                         BindingResult bindingResult,
                                                         Principal principal)
    {
        List<FieldMessage> messages = new ArrayList<>();
        if(bindingResult.hasFieldErrors())
        {
            //Show the user his field errors
            messages = ControllerUtils.parseFieldErrors(bindingResult.getFieldErrors(), "FIELD_ERROR");
        }

        Double latitude = -1., longitude = -1.;
        if(messages.size() == 0)
        {
            try
            {
                latitude = Double.valueOf(locationDTO.getGpsLatitude());
            }
            catch (NumberFormatException exc)
            //Неправильный формат, добавить сообщение об ошибке
            {
                messages.add(new FieldMessage("gpsLatitude", "Please enter a valid number.",
                        "FIELD_ERROR"));
            }

            try
            {
                longitude = Double.valueOf(locationDTO.getGpsLongitude());
            }
            catch (NumberFormatException exc)
            {
                messages.add(new FieldMessage("gpsLongitude", "Please enter a valid number.",
                        "FIELD_ERROR"));
            }
        }

        if(messages.size() == 0)
        {
            if(longitude < -180 || longitude > 180)
            {
                messages.add(new FieldMessage("gpsLongitude",
                        "Longitude must be between -180 and 180.", "COORDINATE_INCORRECT"));
            }

            if(latitude <- 90 || latitude > 90)
            {
                messages.add(new FieldMessage("gpsLatitude",
                        "Latitude must be between -90 and 90.", "COORDINATE_INCORRECT"));
            }
        }

        if(messages.size() == 0)
        {
            String email = principal.getName();
            appUserService.updateCoordinates(email, latitude, longitude);
        }

        return new ResponseEntity<>(new QueryStatus(messages.size() == 0, messages),
                messages.size() == 0 ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }
}
