package ru.itis.fivecg.database.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.itis.fivecg.database.domain.Car;
import ru.itis.fivecg.dto.UserCarDTO;

import java.util.List;

public interface CarRepository extends CrudRepository<Car, Long>
{
    @Query("select c from Car c where c.owner.id = ?1 and c.defaultCar = true")
    UserCarDTO findDefaultCar(long userId);

    Car findByIdentification(String identification);

    List<Car> findByOwner_Id(long userId);

    @Query("select c from Car c where c.defaultCar = true and c.owner.email = ?1")
    Car findDefCarByUserEmail(String email);

    @Query("select c from Car c where c.defaultCar = true and c.owner.cellPhone = ?1")
    Car findDefCarByUserPhone(String phone);

    @Query("select c from Car c where c.defaultCar = true and lower(c.owner.firstName) = lower(?1)"
            + " and lower(c.owner.lastName) = lower(?2)")
    List<Car> findDefCarsByUserName(String firstName, String lastName);

    @Query("select c from Car c")
    List<Car> findAllList();
}
