package ru.itis.fivecg.service.impl;

import com.sun.mail.util.MailSSLSocketFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import ru.itis.fivecg.exception.EmailMalformedException;
import ru.itis.fivecg.exception.EmailWrongFormatException;
import ru.itis.fivecg.service.Email;
import ru.itis.fivecg.service.EmailService;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.security.GeneralSecurityException;
import java.util.Locale;
import java.util.Properties;

/**
 * Реализация сервиса отсылки E-Mail
 */
@Service
public class EmailServiceImpl implements EmailService
{
    @Value("${smtp.host}")
    private String host;
    @Value("${smtp.port}")
    private String port;
    @Value("${smtp.user}")
    private String user;
    @Value("${smtp.password}")
    private String password;
    @Value("${email.emailFrom}")
    private String emailFrom;
    @Value("${home.url}")
    private String homeUrl;

    @Autowired
    private MessageSource messageSource;

    @Override
    public void sendEmail(Email email) throws MessagingException
    {
        if(email == null || email.getFrom() == null || email.getSubject() == null
                || email.getText() == null || email.getTo() == null)
        {
            throw new EmailMalformedException("Объект email и его поля не должны быть равны null.");
        }

        /* TODO: Проверка валидности введенного E-mail */
        if(false)   /* Проверяем правильность форматов адресов To и From */
        {
            throw new EmailWrongFormatException();
        }

        Properties properties = System.getProperties();
        properties.put("mail.transport.protocol", "smtp");
        properties.setProperty("mail.smtp.host", host);
        properties.setProperty("mail.smtp.port", port);
        properties.setProperty("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        MailSSLSocketFactory sf = null;
        try
        {
            sf = new MailSSLSocketFactory();
        }
        catch (GeneralSecurityException e)
        {
            throw new MessagingException("Exception", e);
        }
        sf.setTrustAllHosts(true);
        properties.put("mail.smtp.ssl.socketFactory", sf);

        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                if ((user != null) && (user.length() > 0)
                        && (password != null) && (password.length() > 0)) {
                    return new PasswordAuthentication(user, password);
                }
                return null;
            }
        });
        MimeMessage message = new MimeMessage(session);

        //Устанавливаем настройки электронного сообщения
        message.setFrom(new InternetAddress(email.getFrom()));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email.getTo()));
        message.setSubject(email.getSubject());
        message.setContent(email.getText(), "text/html; charset=utf-8");

        Transport.send(message);
    }

    @Override
    public void sendVerificationEmail(String email, String token) throws MessagingException
    {
        String content = messageSource.getMessage("emailVerification.template",
                /* Аргумент к emailVerification.template - секретный ключ, при присутствии которого
                в этой ссылке при нажатии на нее происходит подтверждение E-Mail */
                new Object[]{ token }, Locale.getDefault());
        String subject = messageSource.getMessage("emailVerification.subject", null, Locale.getDefault());

        Email emailToSend = new Email(emailFrom, email, subject, content);
        sendEmail(emailToSend);
    }


    public void setHost(String host)
    {
        this.host = host;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setMessageSource(MessageSource messageSource)
    {
        this.messageSource = messageSource;
    }
}
