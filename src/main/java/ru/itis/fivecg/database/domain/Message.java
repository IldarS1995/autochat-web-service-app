package ru.itis.fivecg.database.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "message")
public class Message
{
    @Id
    @Column(name = "message_id")
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "sender_id", referencedColumnName = "user_id", nullable = false)
    private AppUser sender;

    @ManyToOne
    @JoinColumn(name = "receiver_id", referencedColumnName = "user_id", nullable = true)
    private AppUser receiver;

    public enum MessageType { SOS, ORDINARY, SYSTEM };
    @Column(name = "message_type", nullable = false)
    private MessageType messageType;

    @Column(name = "created_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "message", nullable = false, length = 1000)
    private String message;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public AppUser getSender()
    {
        return sender;
    }

    public void setSender(AppUser sender)
    {
        this.sender = sender;
    }

    public AppUser getReceiver()
    {
        return receiver;
    }

    public void setReceiver(AppUser receiver)
    {
        this.receiver = receiver;
    }

    public MessageType getMessageType()
    {
        return messageType;
    }

    public void setMessageType(MessageType messageType)
    {
        this.messageType = messageType;
    }

    public Date getCreatedAt()
    {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
