package ru.itis.fivecg.service;

import ru.itis.fivecg.exception.*;

import javax.mail.MessagingException;

/**
 * Интерфейс сервиса, занимающийся отсылкой почты
 * Created by SayfeevI on 18.06.2015.
 */
public interface EmailService
{
    /**
     * Отправить электронное сообщение
     * @throws MessagingException Если не удалось отправить E-Mail
     * @throws EmailMalformedException Если объект email или какое-то из его полей равны <code>null</code>.
     * @throws EmailWrongFormatException Если адреса To или From имеют форматы, не удовлетворяющие нашим
     * требованиям.
     */
    void sendEmail(Email email) throws MessagingException, EmailMalformedException;

    /**
     * Отправить юзеру на почту письмо о подтверждении адреса E-Mail
     * @param email E-Mail, на который отправить письмо и который нужно подтвердить
     * @param token Секретный ключ, который юзеру необходимо отправить
     *              на сервер(например, нажав ссылку с ним в адресной строке)
     */
    void sendVerificationEmail(String email, String token) throws MessagingException;
}
