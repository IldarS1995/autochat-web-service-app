package ru.itis.fivecg.dto;

import java.util.List;

public class QueryStatus
{
    private Boolean success;
    private List<FieldMessage> messages;

    public QueryStatus(Boolean success, List<FieldMessage> messages)
    {
        this.success = success;
        this.messages = messages;
    }

    public Boolean getSuccess()
    {
        return success;
    }

    public List<FieldMessage> getMessages()
    {
        return messages;
    }
}
