package ru.itis.fivecg.database.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "car_photo")
public class CarPhoto
{
    @Id
    @Column(name = "photo_id")
    @GeneratedValue
    private Long photoId;

    @Column(name = "photo", nullable = true)
    private byte[] photo;

    @Column(name = "created_at", nullable = true)
    private Date createdAt;

    @OneToOne
    @JoinColumn(name = "car_id", referencedColumnName = "car_id", nullable = false)
    private Car car;

    public Long getPhotoId()
    {
        return photoId;
    }

    public void setPhotoId(Long photoId)
    {
        this.photoId = photoId;
    }

    public byte[] getPhoto()
    {
        return photo;
    }

    public void setPhoto(byte[] photo)
    {
        this.photo = photo;
    }

    public Date getCreatedAt()
    {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt)
    {
        this.createdAt = createdAt;
    }

    public Car getCar()
    {
        return car;
    }

    public void setCar(Car car)
    {
        this.car = car;
    }
}
