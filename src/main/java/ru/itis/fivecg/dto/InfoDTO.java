package ru.itis.fivecg.dto;

import java.util.List;

public class InfoDTO<T>
{
    private boolean success;
    private List<FieldMessage> messages;
    private T info;

    public InfoDTO() { }
    public InfoDTO(boolean success, List<FieldMessage> messages, T info)
    {
        this.success = success;
        this.messages = messages;
        this.info = info;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public List<FieldMessage> getMessages()
    {
        return messages;
    }

    public T getInfo()
    {
        return info;
    }
}