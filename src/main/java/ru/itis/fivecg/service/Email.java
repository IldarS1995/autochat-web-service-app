package ru.itis.fivecg.service;

/**
 * Класс-оболочка для хранения данных электронного сообщения
 * Created by SayfeevI on 18.06.2015.
 */
public class Email
{
    /** От кого идет сообщение */
    private String from;
    /** Кому идет сообщение */
    private String to;
    /** Тема сообщения */
    private String subject;
    /** Текст сообщения */
    private String text;

    public Email(String from, String to, String subject, String text)
    {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.text = text;
    }

    /** От кого идет сообщение */
    public String getFrom()
    {
        return from;
    }

    /** Кому идет сообщение */
    public String getTo()
    {
        return to;
    }

    /** Тема сообщения */
    public String getSubject()
    {
        return subject;
    }

    /** Текст сообщения */
    public String getText()
    {
        return text;
    }
}
