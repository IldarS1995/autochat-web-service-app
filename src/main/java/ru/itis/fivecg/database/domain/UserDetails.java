package ru.itis.fivecg.database.domain;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "user_details")
public class UserDetails
{
    @Id
    @Column(name = "user_details_id")
    @GeneratedValue
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    private AppUser user;

    public enum Gender {MALE, FEMALE}
    private Gender gender;

    @Column(name = "birth_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;

    @Column(length = 100)
    private String country;

    @Column(length = 100)
    private String city;

    public enum USER_STATUS { ONLINE, OFFLINE, BACKGROUND, BUSY }
    @Column(name = "net_status", length = 15, nullable = false)
    private USER_STATUS netStatus = USER_STATUS.OFFLINE;

    @Column(name = "photo_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date photoDate;

    private byte[] avatar;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public AppUser getUser()
    {
        return user;
    }

    public void setUser(AppUser user)
    {
        this.user = user;
    }

    public Gender getGender()
    {
        return gender;
    }

    public void setGender(Gender gender)
    {
        this.gender = gender;
    }

    public Date getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(Date birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public byte[] getAvatar()
    {
        return avatar;
    }

    public void setAvatar(byte[] avatar)
    {
        this.avatar = avatar;
    }

    public USER_STATUS getNetStatus()
    {
        return netStatus;
    }

    public void setNetStatus(USER_STATUS netStatus)
    {
        this.netStatus = netStatus;
    }

    public Date getPhotoDate()
    {
        return photoDate;
    }

    public void setPhotoDate(Date photoDate)
    {
        this.photoDate = photoDate;
    }
}
