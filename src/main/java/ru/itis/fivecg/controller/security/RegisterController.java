package ru.itis.fivecg.controller.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.itis.fivecg.controller.ControllerUtils;
import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.database.domain.UserActivation;
import ru.itis.fivecg.dto.*;
import ru.itis.fivecg.service.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/security", produces = MediaType.APPLICATION_JSON_VALUE)
public class RegisterController
{
    private enum ErrorCode { FIELD_ERROR, PASSWORDS_NOT_MATCH,
                             EMAIL_ALREADY_EXISTS, PHONE_ALREADY_EXISTS,
                             EMAIL_VERIFICATION_SEND_FAILED, IDENTIFICATION_EXISTS }

    @Autowired
    private AppUserService appUserService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private CarService carService;
    @Autowired
    private EmailActivationService emailActivationService;
    @Autowired
    private AppUserDetailsService appUserDetailsService;

    @RequestMapping(value = "/register", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<QueryStatus> registerPerson(@RequestBody @Valid UserAuthDataDTO user,
                                      BindingResult bindingResult)
    {
        List<FieldMessage> messages = new ArrayList<>();
        if(bindingResult.hasFieldErrors())
        {
            //Show the user his field errors
            messages = ControllerUtils.parseFieldErrors(bindingResult.getFieldErrors(),
                    ErrorCode.FIELD_ERROR.name());
        }

        if(!(user.getPassword() == null || user.getRepeatPassword() == null))
        {
            if (!user.getPassword().equals(user.getRepeatPassword()))
            {
                messages.add(new FieldMessage("password", "Passwords are not equal.",
                        ErrorCode.PASSWORDS_NOT_MATCH.name()));
            }
        }

        if(user.getEmail() != null)
        {
            UserAuthDataDTO userFromDb = appUserService.getUserByEmail(user.getEmail());
            if (userFromDb != null)
            {
                messages.add(new FieldMessage("email", "User with such E-Mail already exists.",
                        ErrorCode.EMAIL_ALREADY_EXISTS.name()));
            }
        }

        if(user.getPhone() != null)
        {
            UserAuthDataDTO userFromDb = appUserService.getByPhoneNumber(user.getPhone());
            if (userFromDb != null)
            {
                messages.add(new FieldMessage("phone", "User with such phone number already exists.",
                        ErrorCode.PHONE_ALREADY_EXISTS.name()));
            }
        }

        if(user.getUserCar() != null && user.getUserCar().getIdentification() != null)
        {
            UserCarDTO car = carService.getCarByIdentification(user.getUserCar().getIdentification());
            if (car != null)
            {
                messages.add(new FieldMessage("car.identification",
                        "Car with such identification already exists.",
                        ErrorCode.IDENTIFICATION_EXISTS.name()));
            }
        }

        if(messages.size() == 0)
            //Ошибок нет, зарегистрировать
        {
            //Сохранить объект пользователя в БД
            AppUser userRet = appUserService.saveUser(user);

            user.getUserCar().setDefaultCar(true);
            carService.addCar(userRet, user.getUserCar());
            appUserDetailsService.addUserAccountDetails(userRet);

        /* Отправление письма пользователю */
            UserActivation userActivation = emailActivationService.getUserActivation(userRet.getId());
            try
            {
                emailService.sendVerificationEmail(user.getEmail(), userActivation.getToken());
            }
            catch (MessagingException e)
            /* Произошла проблема с посылкой сообщения */
            {
                messages.add(new FieldMessage("email", "Error happened while trying to send verification email.", ErrorCode.EMAIL_VERIFICATION_SEND_FAILED.name()));
            }
        }

        boolean success = false;
        if(messages.size() == 0)
        {
            success = true;
        }

        return new ResponseEntity<>(new QueryStatus(success, messages),
                success ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }
}
