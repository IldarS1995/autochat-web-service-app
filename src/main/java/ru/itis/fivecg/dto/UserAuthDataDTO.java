package ru.itis.fivecg.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAuthDataDTO
{
    private Long id;
    @NotNull
    @Email
    private String email;
    @NotNull
    @Length(min = 2, max = 40)
    private String firstName;
    @NotNull
    @Length(min = 2, max = 40)
    private String lastName;
    @NotNull
    @Length(min = 5, max = 15)
    private String phone;
    @NotNull
    @Length(min = 5, max = 100)
    private String password;
    @NotNull
    @Length(min = 5, max = 100)
    private String repeatPassword;
    @NotNull
    private String sendCheckCodeType;

    @Valid
    private UserCarDTO userCar;

    public UserAuthDataDTO() { }
    public UserAuthDataDTO(Long id, String email, String firstName, String lastName, String phone,
                           String password, String repeatPassword, String sendCheckCodeType,
                           UserCarDTO userCar)
    {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.password = password;
        this.repeatPassword = repeatPassword;
        this.sendCheckCodeType = sendCheckCodeType;
        this.userCar = userCar;
    }

    public Long getId()
    {
        return id;
    }

    public String getEmail()
    {
        return email;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getPhone()
    {
        return phone;
    }

    public String getPassword()
    {
        return password;
    }

    public String getRepeatPassword()
    {
        return repeatPassword;
    }

    public String getSendCheckCodeType()
    {
        return sendCheckCodeType;
    }

    public UserCarDTO getUserCar()
    {
        return userCar;
    }
}
