package ru.itis.fivecg.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class NotActivatedException extends RuntimeException
{
    public NotActivatedException() { }
    public NotActivatedException(String msg) { super(msg); }
}
