package ru.itis.fivecg.dto;

public class UserDetailsDTO
{
    private Long userId;
    private String gender;
    private String birthDate;
    private String country;
    private String city;
    private String netStatus;

    public UserDetailsDTO() { }
    public UserDetailsDTO(Long userId, String gender, String birthDate,
                          String country, String city, String netStatus)
    {
        this.userId = userId;
        this.gender = gender;
        this.birthDate = birthDate;
        this.country = country;
        this.city = city;
        this.netStatus = netStatus;
    }

    public Long getUserId()
    {
        return userId;
    }

    public String getGender()
    {
        return gender;
    }

    public String getBirthDate()
    {
        return birthDate;
    }

    public String getCountry()
    {
        return country;
    }

    public String getCity()
    {
        return city;
    }

    public String getNetStatus()
    {
        return netStatus;
    }
}
