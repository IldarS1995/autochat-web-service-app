package ru.itis.fivecg.service.impl.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.fivecg.database.dao.AppUserRepository;
import ru.itis.fivecg.database.dao.CarRepository;
import ru.itis.fivecg.database.dao.UserActivationRepository;
import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.database.domain.Car;
import ru.itis.fivecg.database.domain.UserActivation;
import ru.itis.fivecg.database.domain.UserDetails;
import ru.itis.fivecg.dto.SearchInfoDTO;
import ru.itis.fivecg.dto.UserAuthDataDTO;
import ru.itis.fivecg.service.AppUserDetailsService;
import ru.itis.fivecg.service.AppUserService;
import ru.itis.fivecg.service.EmailActivationService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static ru.itis.fivecg.dto.DtoUtil.convertFromDto;
import static ru.itis.fivecg.dto.DtoUtil.convertToDto;

@Service
@Transactional
public class AppUserServiceImpl implements AppUserService
{
    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private UserActivationRepository userActivationRepository;
    @Autowired
    private AppUserDetailsService appUserDetailsService;
    @Autowired
    private EmailActivationService emailActivationService;
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private MessageDigestPasswordEncoder encoder;

    @Override
    public UserAuthDataDTO getUserByEmail(String email)
    {
        if(email == null)
        {
            throw new IllegalArgumentException("E-Mail mustn't be null.");
        }

        return convertToDto(appUserRepository.findByEmail(email));
    }

    @Override
    public UserAuthDataDTO getByPhoneNumber(String phone)
    {
        if(phone == null)
        {
            throw new IllegalArgumentException("User's phone number mustn't be null.");
        }

        return convertToDto(appUserRepository.findByCellPhone(phone));
    }

    @Override
    public AppUser saveUser(UserAuthDataDTO user)
    {
        AppUser u = convertFromDto(user);
        u.setPassword(encoder.encodePassword(u.getPassword(), null));
        AppUser ret = appUserRepository.save(u);

        UserDetails userDetails = new UserDetails();
        userDetails.setUser(u);
        appUserDetailsService.addDetails(userDetails);

        emailActivationService.addUserActivationForUser(u);

        return ret;
    }

    @Override
    public AppUser getUser(long userId)
    {
        return appUserRepository.findOne(userId);
    }

    @Override
    public List<SearchInfoDTO> searchUsers(SearchInfoDTO si)
    {
        if(si == null || (si.getFirstName() == null && si.getLastName() == null
            && si.getDistance() == null && si.getEmail() == null && si.getIdentification() == null
            && si.getPhone() == null))
        {
            throw new IllegalArgumentException();
        }

        List<Car> defaultUserCars = null;
        Car userCar = null;
        if(!empty(si.getFirstName()) && !empty(si.getLastName()))
        {
            defaultUserCars = carRepository.findDefCarsByUserName(si.getFirstName(), si.getLastName());
        }
        else if(!empty(si.getDistance()))
        {
            //TODO: Переписать
            defaultUserCars = carRepository.findAllList();
        }
        else if(!empty(si.getEmail()))
        {
            userCar = carRepository.findDefCarByUserEmail(si.getEmail());
        }
        else if(!empty(si.getPhone()))
        {
            userCar = carRepository.findDefCarByUserPhone(si.getPhone());
        }
        else if(!empty(si.getIdentification()))
        {
            userCar = carRepository.findByIdentification(si.getIdentification());
        }

        if(defaultUserCars == null && userCar == null)
            //Не найдено данных вообще
        {
            defaultUserCars = new ArrayList<>();
        }

        if(userCar != null)
            //Найден единичный юзер
        {
            defaultUserCars = Collections.singletonList(userCar);
        }

        List<SearchInfoDTO> result = new ArrayList<>();
        for(Car car : defaultUserCars)
        {
            AppUser user = car.getOwner();
            result.add(convertToDto(user, car.getIdentification()));
        }

        return result;
    }

    @Override
    public void updateCoordinates(String email, Double latitude, Double longitude)
    {
        AppUser appUser = appUserRepository.findByEmail(email);
        appUser.setCurrentLatitude(latitude);
        appUser.setCurrentLongitude(longitude);
    }

    @Override
    public UserActivation getActivationByEmail(String email)
    {
        return userActivationRepository.findByUser_Email(email);
    }

    @Override
    public void saveActivation(UserActivation activation)
    {
        userActivationRepository.save(activation);
    }

    private boolean empty(String str)
    {
        return str == null || str.trim().length() == 0;
    }
}
