package ru.itis.fivecg.service.impl.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.fivecg.database.dao.CarPhotoRepository;
import ru.itis.fivecg.database.dao.CarRepository;
import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.database.domain.Car;
import ru.itis.fivecg.database.domain.CarPhoto;
import ru.itis.fivecg.dto.UserCarDTO;
import ru.itis.fivecg.service.CarService;

import java.util.List;

import static ru.itis.fivecg.dto.DtoUtil.convertFromDto;
import static ru.itis.fivecg.dto.DtoUtil.convertToDto;

@Service
@Transactional
public class CarServiceImpl implements CarService
{
    @Autowired
    private CarRepository carRepository;
    @Autowired
    private CarPhotoRepository carPhotoRepository;

    @Override
    public Long addCar(AppUser user, UserCarDTO car)
    {
        if (user == null || car == null)
        {
            throw new IllegalArgumentException("User and car arguments mustn't be null.");
        }

        Car carToSave = convertFromDto(user, car);
        carRepository.save(carToSave);

        CarPhoto carPhoto = new CarPhoto();
        carPhoto.setCar(carToSave);
        carPhotoRepository.save(carPhoto);

        return carToSave.getCarId();
    }

    @Override
    public UserCarDTO getDefaultCar(long userId)
    {
        return carRepository.findDefaultCar(userId);
    }

    @Override
    public List<UserCarDTO> getUsersCars(long userId)
    {
        return null;
    }

    @Override
    public UserCarDTO getCarByIdentification(String identification)
    {
        return convertToDto(carRepository.findByIdentification(identification));
    }
}
