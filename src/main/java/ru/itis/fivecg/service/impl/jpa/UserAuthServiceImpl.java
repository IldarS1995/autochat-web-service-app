package ru.itis.fivecg.service.impl.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.fivecg.database.dao.AppUserRepository;
import ru.itis.fivecg.database.dao.UserActivationRepository;
import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.database.domain.UserActivation;
import ru.itis.fivecg.exception.NotActivatedException;
import ru.itis.fivecg.service.AppUserDetailsService;
import ru.itis.fivecg.service.AppUserService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class UserAuthServiceImpl implements UserDetailsService
{
    @Autowired
    private AppUserRepository appUserRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException
    {
        AppUser appUser = appUserRepository.findByEmail(email);
        if(appUser == null)
        {
            throw new UsernameNotFoundException("The specified E-Mail not found.");
        }

        return new User(email, appUser.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
    }
}
