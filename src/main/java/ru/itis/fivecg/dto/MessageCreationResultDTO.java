package ru.itis.fivecg.dto;

public class MessageCreationResultDTO
{
    private Long id;
    private String createdAt;

    public MessageCreationResultDTO() { }
    public MessageCreationResultDTO(Long id, String createdAt)
    {
        this.id = id;
        this.createdAt = createdAt;
    }

    public Long getId()
    {
        return id;
    }

    public String getCreatedAt()
    {
        return createdAt;
    }
}
