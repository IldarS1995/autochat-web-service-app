package ru.itis.fivecg.dto;

import javax.validation.constraints.NotNull;

public class LocationDTO
{
    @NotNull
    private String gpsLongitude;
    @NotNull
    private String gpsLatitude;

    public LocationDTO() { }
    public LocationDTO(String gpsLongitude, String gpsLatitude)
    {
        this.gpsLongitude = gpsLongitude;
        this.gpsLatitude = gpsLatitude;
    }

    public String getGpsLongitude()
    {
        return gpsLongitude;
    }

    public String getGpsLatitude()
    {
        return gpsLatitude;
    }
}
