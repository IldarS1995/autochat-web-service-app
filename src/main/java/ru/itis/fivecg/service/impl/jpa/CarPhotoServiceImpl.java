package ru.itis.fivecg.service.impl.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.fivecg.database.dao.CarPhotoRepository;
import ru.itis.fivecg.database.domain.CarPhoto;
import ru.itis.fivecg.service.CarPhotoService;

@Service
@Transactional
public class CarPhotoServiceImpl implements CarPhotoService
{
    @Autowired
    private CarPhotoRepository carPhotoRepository;

    @Override
    public CarPhoto getPhotoOfCar(long carId)
    {
        return carPhotoRepository.findByCar_CarId(carId);
    }

    @Override
    public void updatePhoto(long carId, byte[] photoBytes)
    {
        CarPhoto photo = carPhotoRepository.findByCar_CarId(carId);
        photo.setPhoto(photoBytes);
    }
}
