package ru.itis.fivecg.database.dao;

import org.springframework.data.repository.CrudRepository;
import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.database.domain.UserDetails;

public interface UserDetailsRepository extends CrudRepository<UserDetails, Long>
{
    UserDetails findByUser_Id(Long id);

    UserDetails findByUser_Email(String email);
}
