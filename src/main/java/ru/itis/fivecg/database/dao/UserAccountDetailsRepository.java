package ru.itis.fivecg.database.dao;

import org.springframework.data.repository.CrudRepository;
import ru.itis.fivecg.database.domain.UserAccountDetails;

public interface UserAccountDetailsRepository extends CrudRepository<UserAccountDetails, Long>
{
    UserAccountDetails findByUser_Id(long userId);
}
