package ru.itis.fivecg.dto;

import javax.validation.constraints.NotNull;

public class UserIdDto
{
    @NotNull
    private Long userId;

    public Long getUserId()
    {
        return userId;
    }
}