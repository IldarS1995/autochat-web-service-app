package ru.itis.fivecg.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.fivecg.controller.ControllerUtils;
import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.dto.FieldMessage;
import ru.itis.fivecg.dto.InfoDTO;
import ru.itis.fivecg.dto.MessageCreationResultDTO;
import ru.itis.fivecg.dto.MessageDTO;
import ru.itis.fivecg.service.AppUserService;
import ru.itis.fivecg.service.MessageService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/message")
public class MessageController
{
    @Autowired
    private MessageService messageService;
    @Autowired
    private AppUserService appUserService;

    @RequestMapping(value = "send", method = RequestMethod.POST)
    public ResponseEntity<InfoDTO<MessageCreationResultDTO>> sendMessage(@RequestBody @Valid MessageDTO messageDTO,
                                                       BindingResult bindingResult,
                                                       Principal principal)
    {
        List<FieldMessage> messages = new ArrayList<>();
        if(bindingResult.hasFieldErrors())
        {
            messages = ControllerUtils.parseFieldErrors(bindingResult.getFieldErrors(), "FIELD_ERROR");
        }

        String senderEmail = principal.getName();

        AppUser receiver = null;
        if(messages.size() == 0 && messageDTO.getReceiverId() != null)
        {
            receiver = appUserService.getUser(messageDTO.getReceiverId());
            if(receiver == null)
                //Receiver doesn't exist
            {
                messages.add(new FieldMessage("receiverId", "Receiver doesn't exist.",
                        "USER_NOT_EXISTS"));
            }
        }

        if(receiver == null && messageDTO.getMessageType().equals("ORDINARY"))
        {
            messages.add(new FieldMessage("receiverId", "Ordinary message requires a receiver.",
                    "USER_NOT_SENT"));
        }

        MessageCreationResultDTO message = null;
        if(messages.size() == 0)
        {
            message = messageService.createMessage(senderEmail, messageDTO, receiver);
        }

        return new ResponseEntity<>(new InfoDTO<>(messages.size() == 0, messages, message),
                messages.size() == 0 ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }
}
