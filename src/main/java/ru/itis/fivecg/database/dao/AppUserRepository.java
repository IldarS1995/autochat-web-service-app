package ru.itis.fivecg.database.dao;

import org.springframework.data.repository.CrudRepository;
import ru.itis.fivecg.database.domain.AppUser;

public interface AppUserRepository extends CrudRepository<AppUser, Long>
{
    AppUser findByEmail(String email);

    AppUser findByCellPhone(String phone);
}
