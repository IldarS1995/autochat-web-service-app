package ru.itis.fivecg.database.dao;

import org.springframework.data.repository.CrudRepository;
import ru.itis.fivecg.database.domain.Message;

public interface MessageRepository extends CrudRepository<Message, Long>
{

}
