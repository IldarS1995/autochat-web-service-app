package ru.itis.fivecg.database.domain;

import javax.persistence.*;

@Entity
@Table(name = "car")
public class Car
{
    @Id
    @Column(name = "car_id")
    @GeneratedValue
    private Long carId;

    @ManyToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "user_id", nullable = false)
    private AppUser owner;

    @OneToOne(mappedBy = "car")
    private CarPhoto photo;

    @Column(length = 25, nullable = false)
    private String brand;

    @Column(length = 25, nullable = false)
    private String model;

    @Column(length = 20, nullable = false, unique = true)
    private String identification;

    @Column(length = 20, nullable = false)
    private String color;

    @Column(name = "default_car")
    private Boolean defaultCar;

    public Long getCarId()
    {
        return carId;
    }

    public void setCarId(Long carId)
    {
        this.carId = carId;
    }

    public AppUser getOwner()
    {
        return owner;
    }

    public void setOwner(AppUser owner)
    {
        this.owner = owner;
    }

    public CarPhoto getPhoto()
    {
        return photo;
    }

    public void setPhoto(CarPhoto photo)
    {
        this.photo = photo;
    }

    public String getBrand()
    {
        return brand;
    }

    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public String getIdentification()
    {
        return identification;
    }

    public void setIdentification(String identification)
    {
        this.identification = identification;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public Boolean getDefaultCar()
    {
        return defaultCar;
    }

    public void setDefaultCar(Boolean defaultCar)
    {
        this.defaultCar = defaultCar;
    }
}
