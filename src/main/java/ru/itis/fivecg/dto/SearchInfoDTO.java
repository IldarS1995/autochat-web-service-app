package ru.itis.fivecg.dto;

public class SearchInfoDTO
{
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String identification;
    private String distance;

    public SearchInfoDTO() { }
    public SearchInfoDTO(String id, String firstName, String lastName, String email, String phone, String identification, String distance)
    {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.identification = identification;
        this.distance = distance;
    }

    public String getId()
    {
        return id;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getEmail()
    {
        return email;
    }

    public String getPhone()
    {
        return phone;
    }

    public String getIdentification()
    {
        return identification;
    }

    public String getDistance()
    {
        return distance;
    }
}
