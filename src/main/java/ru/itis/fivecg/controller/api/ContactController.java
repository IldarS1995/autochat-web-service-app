package ru.itis.fivecg.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.itis.fivecg.controller.ControllerUtils;
import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.database.domain.Contact;
import ru.itis.fivecg.dto.*;
import ru.itis.fivecg.service.AppUserService;
import ru.itis.fivecg.service.ContactService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/contact")
public class ContactController
{
    @Autowired
    private AppUserService appUserService;
    @Autowired
    private ContactService contactService;

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public ResponseEntity<InfoDTO<List<UserContactDTO>>> getUserContacts(Principal principal)
    {
        String email = principal.getName();
        List<UserContactDTO> contacts = contactService.getContactsByOwnerEmail(email);
        return new ResponseEntity<>(
                new InfoDTO<>(true, new ArrayList<FieldMessage>(), contacts), HttpStatus.OK
        );
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public ResponseEntity<QueryStatus> addContact(@RequestBody @Valid UserIdDto userIdDto,
                                                  BindingResult bindingResult,
                                                  Principal principal)
    {
        List<FieldMessage> messages = new ArrayList<>();
        if(bindingResult.hasFieldErrors())
        {
            messages = ControllerUtils.parseFieldErrors(bindingResult.getFieldErrors(),
                    "FIELD_ERROR");
        }

        String email = principal.getName();
        Long userId = userIdDto.getUserId();

        if(messages.size() == 0)
        {
            AppUser user = appUserService.getUser(userId);
            if (user == null)
                //User with such ID wasn't found
            {
                messages.add(new FieldMessage("userId",
                        "Such user doesn't exist.", "USER_NOT_EXISTS"));
            }
        }

        if(messages.size() == 0)
        {
            Contact contact = contactService.getContact(email, userId);
            if(contact != null)
            {
                messages.add(new FieldMessage("userId",
                        "This user is already in your contacts.", "USER_ALREADY_PRESENT"));
            }
        }

        if(messages.size() == 0)
        {
            contactService.addContact(email, userId);
        }

        return new ResponseEntity<>(new QueryStatus(messages.size() == 0, messages),
                messages.size() == 0 ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public ResponseEntity<QueryStatus> deleteContact(@RequestBody @Valid UserIdDto userIdDto,
                                                     BindingResult bindingResult,
                                                     Principal principal)
    {
        List<FieldMessage> messages = new ArrayList<>();
        if(bindingResult.hasFieldErrors())
        {
            messages = ControllerUtils.parseFieldErrors(bindingResult.getFieldErrors(),
                    "FIELD_ERROR");
        }

        Long userId = userIdDto.getUserId();

        Contact contact = null;
        if(messages.size() == 0)
        {
            contact = contactService.getContact(principal.getName(), userId);
            if(contact == null)
            {
                messages.add(new FieldMessage("userId", "This user is not in your contacts list.",
                        "USER_NOT_PRESENT"));
            }
        }

        if(messages.size() == 0)
        {
            contactService.deleteContact(contact);
        }

        return new ResponseEntity<>(new QueryStatus(messages.size() == 0, messages),
                messages.size() == 0 ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }
}
