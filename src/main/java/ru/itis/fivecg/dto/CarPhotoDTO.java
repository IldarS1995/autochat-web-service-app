package ru.itis.fivecg.dto;

public class CarPhotoDTO
{
    private Long id;
    private String createdAt;

    public CarPhotoDTO() { }
    public CarPhotoDTO(Long id, String createdAt)
    {
        this.id = id;
        this.createdAt = createdAt;
    }

    public Long getId()
    {
        return id;
    }

    public String getCreatedAt()
    {
        return createdAt;
    }
}
