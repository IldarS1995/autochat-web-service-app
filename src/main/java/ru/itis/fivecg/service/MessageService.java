package ru.itis.fivecg.service;

import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.dto.MessageCreationResultDTO;
import ru.itis.fivecg.dto.MessageDTO;

public interface MessageService
{
    MessageCreationResultDTO createMessage(String senderEmail, MessageDTO messageDTO, AppUser receiver);
}
