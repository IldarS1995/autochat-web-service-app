package ru.itis.fivecg.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Spring Security configuration for this web app
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AuthConfig extends WebSecurityConfigurerAdapter
{
    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private RestAuthenticationSuccessHandler successHandler;
    @Autowired
    private RestFailureHandler failureHandler;
    @Autowired
    private RestAuthenticationEntryPoint entryPoint;

    @Bean
    public MessageDigestPasswordEncoder encoder()
    {
        //Using MD5 algorithm for hashing passwords
        return new Md5PasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
        auth.userDetailsService(userDetailsService).passwordEncoder(encoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.httpBasic().authenticationEntryPoint(entryPoint)
                .and().csrf().disable().
                formLogin().successHandler(successHandler)
                            .failureHandler(failureHandler).
                loginProcessingUrl("/security/login").
                usernameParameter("username").passwordParameter("password")
            .and().logout().logoutUrl("/security/logout")
                .logoutSuccessUrl("/security/afterLogout")
            .and().authorizeRequests().
                antMatchers("/security/**").anonymous().
                antMatchers("/api/**").hasRole("USER");
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean()
            throws Exception
    {
        return super.authenticationManagerBean();
    }
}
