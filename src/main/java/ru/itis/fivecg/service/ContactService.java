package ru.itis.fivecg.service;

import ru.itis.fivecg.database.domain.Contact;
import ru.itis.fivecg.dto.UserContactDTO;

import java.util.List;

public interface ContactService
{
    List<UserContactDTO> getContactsByOwnerEmail(String email);

    Contact getContact(String email, Long userId);

    void addContact(String email, Long userId);

    void deleteContact(Contact contact);
}
