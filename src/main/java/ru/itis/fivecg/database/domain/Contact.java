package ru.itis.fivecg.database.domain;

import javax.persistence.*;

@Entity
@Table(name = "contact",
        uniqueConstraints = @UniqueConstraint(columnNames = {"owner_id", "another_user_id"}))
public class Contact
{
    @Id
    @Column(name = "contact_id")
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "user_id", nullable = false)
    private AppUser contactOwner;
    @ManyToOne
    @JoinColumn(name = "another_user_id", referencedColumnName = "user_id", nullable = false)
    private AppUser anotherUser;

    public enum ContactStatus { UNCONFIRMED, REQUEST, REFUSED, FRIEND, BLOCKED, DELETED }
    @Column(name = "contact_status", nullable = false)
    private ContactStatus contactStatus;

    public Long getId()
    {
        return id;
    }

    public ContactStatus getContactStatus()
    {
        return contactStatus;
    }

    public void setContactStatus(ContactStatus contactStatus)
    {
        this.contactStatus = contactStatus;
    }

    public AppUser getContactOwner()
    {
        return contactOwner;
    }

    public void setContactOwner(AppUser contactOwner)
    {
        this.contactOwner = contactOwner;
    }

    public AppUser getAnotherUser()
    {
        return anotherUser;
    }

    public void setAnotherUser(AppUser anotherUser)
    {
        this.anotherUser = anotherUser;
    }
}
