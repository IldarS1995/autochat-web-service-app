package ru.itis.fivecg.dto;

public class UserContactDTO
{
    private Long id;
    private String phone;
    private String email;
    private String userFullName;
    private String userPhotoDate;
    private String userNetStatus;
    private String contactStatus;

    public UserContactDTO() { }
    public UserContactDTO(Long id, String phone, String email, String userFullName,
                          String userPhotoDate, String userNetStatus, String contactStatus)
    {
        this.id = id;
        this.phone = phone;
        this.email = email;
        this.userFullName = userFullName;
        this.userPhotoDate = userPhotoDate;
        this.userNetStatus = userNetStatus;
        this.contactStatus = contactStatus;
    }

    public Long getId()
    {
        return id;
    }

    public String getPhone()
    {
        return phone;
    }

    public String getEmail()
    {
        return email;
    }

    public String getUserFullName()
    {
        return userFullName;
    }

    public String getUserPhotoDate()
    {
        return userPhotoDate;
    }

    public String getUserNetStatus()
    {
        return userNetStatus;
    }

    public String getContactStatus()
    {
        return contactStatus;
    }
}
