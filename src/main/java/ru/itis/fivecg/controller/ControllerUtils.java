package ru.itis.fivecg.controller;

import org.springframework.validation.FieldError;
import ru.itis.fivecg.dto.FieldMessage;

import java.util.ArrayList;
import java.util.List;

public final class ControllerUtils
{
    private ControllerUtils() { }

    public static List<FieldMessage> parseFieldErrors(List<FieldError> lst, String statusCode)
    {
        List<FieldMessage> messages = new ArrayList<>();
        for(FieldError err : lst)
        {
            messages.add(new FieldMessage(err.getField(), err.getDefaultMessage(), statusCode));
        }

        return messages;
    }
}
