package ru.itis.fivecg.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class UserCarDTO
{
    /** Марка авто */
    @NotNull
    @Length(min = 2, max = 25)
    private String brand;
    /** Модель */
    @NotNull
    @Length(min = 2, max = 25)
    private String model;
    /** Номер автомобился */
    @NotNull
    @Length(min = 5, max = 20)
    private String identification;
    /** Цвет кузова */
    @NotNull
    @Length(min = 2, max = 20)
    private String color;
    /** Является ли авто автомобилем по умолчанию данного юзера */
    private boolean defaultCar;

    //Фото автомобиля
    private CarPhotoDTO attachment;

    public UserCarDTO() { }
    public UserCarDTO(String brand, String model, String identification, String color,
                      CarPhotoDTO attachment)
    {
        this.brand = brand;
        this.model = model;
        this.identification = identification;
        this.color = color;
        this.attachment = attachment;
    }

    public CarPhotoDTO getAttachment()
    {
        return attachment;
    }

    public String getBrand()
    {
        return brand;
    }

    public String getModel()
    {
        return model;
    }

    public String getIdentification()
    {
        return identification;
    }

    public String getColor()
    {
        return color;
    }

    public boolean isDefaultCar()
    {
        return defaultCar;
    }

    public void setDefaultCar(boolean defaultCar)
    {
        this.defaultCar = defaultCar;
    }
}
