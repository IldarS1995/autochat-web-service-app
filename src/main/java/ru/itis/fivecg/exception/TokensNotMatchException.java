package ru.itis.fivecg.exception;

/**
 * Исключение, которое возникает, когда при подтверждении E-Mail
 * переданный ключ и ключ, хранящийся в БД, не совпадают
 */
public class TokensNotMatchException extends Exception
{
    public TokensNotMatchException()
    {
    }
    public TokensNotMatchException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
