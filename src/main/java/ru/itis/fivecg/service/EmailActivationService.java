package ru.itis.fivecg.service;

import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.database.domain.UserActivation;
import ru.itis.fivecg.exception.*;

import javax.mail.MessagingException;

public interface EmailActivationService
{
    /** Вовзращает экземпляр UserActivation по ID пользователя
     * @param userId ID пользователя
     */
    UserActivation getUserActivation(long userId);

    /**
     * Зарегистрировать подтверждение пользователем своего E-Mail
     * @param userId ID пользователя, чем E-Mail подтверждается
     * @throws EmailNotActivatedViaTokenException Если до этого не было произведено
     * подтверждение через передачу секретного ключа на сервер
     */
    void activateEmailOfUser(long userId) throws EmailNotActivatedViaTokenException;

    /**
     * Проверить, совпадает ли переданный юзером ключ с ключом, хранящимся в БД
     * @param email E-Mail пользователя
     * @param token Ключ подтверждения
     * @throws TokensNotMatchException Если переданный ключ и ключ, хранящийся в БД
     * для этого объекта активации, не совпадают
     * @throws IllegalArgumentException Если какой-либо из аргументов <code>email</code>
     * или <code>token</code> равен <code>null</code>
     */
    void checkActivationToken(String email, String token) throws TokensNotMatchException;

    /**
     * Вернуть объект UserActivation по адресу E-Mail юзера, соответствующего этому объекту UserActivation
     * @throws IllegalArgumentException Если аргумент email равен <code>null</code>
     */
    UserActivation getUserActivationByEmail(String email);

    /**
     * Удалить объект UserActivation, соответствующий данному пользователю
     * @throws IllegalArgumentException Если объект User или его поле ID являются <code>null</code>.
     */
    void deleteUserActivationOfUser(AppUser user);

    /**
     * Сгенерировать секретный ключ подтверждения и отправить его данному юзеру на его E-Mail
     * для подтверждения адреса почты
     * @throws MessagingException если при попытке отправить на данный E-Mail
     * сообщение о подтверждении возникла ошибка
     * @throws IllegalArgumentException Если объект user или его поле email равны <code>null</code>.
     */
    void generateTokenAndSendVerificationEmailToUser(AppUser user) throws MessagingException;

    /**
     * Создать объект UserActivation для данного юзера с инициализированным ключом подтверждения
     * @throws IllegalArgumentException Если объект User или его поле ID являются <code>null</code>.
     */
    UserActivation addUserActivationForUser(AppUser user);

}
