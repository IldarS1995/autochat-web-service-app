package ru.itis.fivecg.config;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import ru.itis.fivecg.database.domain.UserActivation;
import ru.itis.fivecg.service.AppUserService;

/**
 * This will call once the request is authenticated. If it is not, the request
 * will be redirected to authenticate entry point
 */
@Component
public class RestAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler
{
    @Autowired
    private AppUserService appUserService;

    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request,
                                        final HttpServletResponse response,
                                        final Authentication authentication)
            throws ServletException, IOException
    {
        clearAuthenticationAttributes(request);

        User user = (User)authentication.getPrincipal();
        UserActivation activation = appUserService.getActivationByEmail(user.getUsername());
        if(!activation.getActivatedViaToken())
        {
            authentication.setAuthenticated(false);
            response.setStatus(403);
            response.setHeader("Content-Type", "application/json;charset=utf-8");
            PrintWriter writer = response.getWriter();
            writer.println("{success: false, messages: [ {field: \"email\", " +
                    "message: \"Your account is not yet activated.\", " +
                    "statusCode: \"ACC_NOT_ACTIVATED\"} ]}");
            return;
        }

        activation.setActivatedViaLogin(true);
        appUserService.saveActivation(activation);

        clearAuthenticationAttributes(request);
    }
}