package ru.itis.fivecg.controller.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.fivecg.controller.ControllerUtils;
import ru.itis.fivecg.database.domain.UserActivation;
import ru.itis.fivecg.dto.AccountActivationDTO;
import ru.itis.fivecg.dto.FieldMessage;
import ru.itis.fivecg.dto.QueryStatus;
import ru.itis.fivecg.exception.TokensNotMatchException;
import ru.itis.fivecg.service.EmailActivationService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/security")
public class VerificationController
{
    private enum ActivationStatus { WRONG_EMAIL, ALREADY_ACTIVATED, TOKENS_NOT_MATCH, FIELD_ERROR }

    @Autowired
    private EmailActivationService emailActivationService;

    @RequestMapping(value = "activate", method = RequestMethod.POST)
    public ResponseEntity<QueryStatus> activateEmail(@RequestBody @Valid AccountActivationDTO activation,
                                     BindingResult bindingResult)
    {
        List<FieldMessage> messages = new ArrayList<>();
        if(bindingResult.hasFieldErrors())
        {
            //Show the user his field errors
            messages = ControllerUtils.parseFieldErrors(bindingResult.getFieldErrors(),
                    ActivationStatus.FIELD_ERROR.name());
        }

        if(activation.getEmailOrPhone() != null && activation.getCheckCode() != null)
        {
            UserActivation userActivation = emailActivationService
                    .getUserActivationByEmail(activation.getEmailOrPhone());
            if (userActivation == null)
            /* Объект активации для такого E-Mail не найден */
            {
                messages.add(new FieldMessage("email", "Wrong email.",
                        ActivationStatus.WRONG_EMAIL.name()));
            }

            if (userActivation != null && userActivation.getActivatedViaToken())
            /*Юзер уже подтвердил до этого E-Mail через передачу ключа */
            {
                messages.add(new FieldMessage("email", "Already activated.",
                        ActivationStatus.ALREADY_ACTIVATED.name()));
            }

            if (messages.size() == 0)
            //Ошибок нет, подтвердить аккаунт
            {
                try
                {
                    emailActivationService.checkActivationToken(activation.getEmailOrPhone(),
                            activation.getCheckCode());
                }
                catch (TokensNotMatchException e)
                /* Ключи не совпадают */
                {
                    messages.add(new FieldMessage("checkCode",
                            "Generated and received tokens don't match.",
                            ActivationStatus.TOKENS_NOT_MATCH.name()));
                }
            }
        }

        //Первый шаг активации успешно пройден, теперь этому юзеру необходимо
        //залогиниться для окончательного подтверждения E-Mail
        boolean success = false;
        if(messages.size() == 0)
        {
            success = true;
        }

        return new ResponseEntity<>(new QueryStatus(success, messages),
                success ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }
}
