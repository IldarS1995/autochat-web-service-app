package ru.itis.fivecg.service;

import ru.itis.fivecg.database.domain.CarPhoto;

public interface CarPhotoService
{
    CarPhoto getPhotoOfCar(long carId);
    void updatePhoto(long carId, byte[] photo);
}
