package ru.itis.fivecg.dto;

import org.hibernate.validator.constraints.Length;
import ru.itis.fivecg.database.domain.Message;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class MessageDTO
{
    @Min(0)
    private Long receiverId;
    @NotNull
    @Length(min = 1, max = 1000)
    private String message;
    @NotNull
    @Pattern(regexp = "ORDINARY|SOS")
    private String messageType;

    public MessageDTO() { }
    public MessageDTO(Long receiverId, String message, String messageType)
    {
        this.receiverId = receiverId;
        this.message = message;
        this.messageType = messageType;
    }

    public Long getReceiverId()
    {
        return receiverId;
    }

    public String getMessage()
    {
        return message;
    }

    public String getMessageType()
    {
        return messageType;
    }
}
