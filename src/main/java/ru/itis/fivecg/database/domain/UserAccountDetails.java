package ru.itis.fivecg.database.domain;

import javax.persistence.*;

@Entity
@Table(name = "user_account_details")
public class UserAccountDetails
{
    @Id
    @Column(name = "account_details_id")
    @GeneratedValue
    private Long accountDetailsId;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    private AppUser user;

    @Column(name = "show_location")
    private boolean showLocation;
    @Column(name = "show_cars")
    private boolean showCars;
    @Column(name = "show_user")
    private boolean showUser;

    public UserAccountDetails() { }
    public UserAccountDetails(AppUser user)
    {
        this.user = user;
    }

    @PrePersist
    public void prePersist()
    {
        showUser = showLocation = true;
        showCars = false;
    }

    public Long getAccountDetailsId()
    {
        return accountDetailsId;
    }

    public void setAccountDetailsId(Long accountDetailsId)
    {
        this.accountDetailsId = accountDetailsId;
    }

    public AppUser getUser()
    {
        return user;
    }

    public void setUser(AppUser user)
    {
        this.user = user;
    }

    public boolean isShowLocation()
    {
        return showLocation;
    }

    public void setShowLocation(boolean showLocation)
    {
        this.showLocation = showLocation;
    }

    public boolean isShowCars()
    {
        return showCars;
    }

    public void setShowCars(boolean showCars)
    {
        this.showCars = showCars;
    }

    public boolean isShowUser()
    {
        return showUser;
    }

    public void setShowUser(boolean showUser)
    {
        this.showUser = showUser;
    }
}
