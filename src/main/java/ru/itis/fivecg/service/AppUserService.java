package ru.itis.fivecg.service;

import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.database.domain.UserActivation;
import ru.itis.fivecg.dto.SearchInfoDTO;
import ru.itis.fivecg.dto.UserAuthDataDTO;
import ru.itis.fivecg.dto.UserInfo;

import java.util.List;

public interface AppUserService
{
    UserAuthDataDTO getUserByEmail(String email);

    UserAuthDataDTO getByPhoneNumber(String phone);

    AppUser saveUser(UserAuthDataDTO user);

    AppUser getUser(long userId);

    List<SearchInfoDTO> searchUsers(SearchInfoDTO searchInfo);

    void updateCoordinates(String email, Double latitude, Double longitude);

    UserActivation getActivationByEmail(String email);

    void saveActivation(UserActivation activation);
}
