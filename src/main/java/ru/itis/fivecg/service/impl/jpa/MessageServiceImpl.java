package ru.itis.fivecg.service.impl.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itis.fivecg.database.dao.AppUserRepository;
import ru.itis.fivecg.database.dao.MessageRepository;
import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.database.domain.Message;
import ru.itis.fivecg.dto.MessageCreationResultDTO;
import ru.itis.fivecg.dto.MessageDTO;
import ru.itis.fivecg.service.MessageService;

import java.util.Date;

import static ru.itis.fivecg.dto.DtoUtil.convertToDto;

@Service
@Transactional
public class MessageServiceImpl implements MessageService
{
    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private MessageRepository messageRepository;

    @Override
    public MessageCreationResultDTO createMessage(String senderEmail, MessageDTO messageDTO, AppUser receiver)
    {
        Message message = new Message();
        message.setSender(appUserRepository.findByEmail(senderEmail));
        message.setReceiver(receiver);
        message.setMessage(messageDTO.getMessage());
        message.setCreatedAt(new Date());
        message.setMessageType(Message.MessageType.valueOf(messageDTO.getMessageType()));

        return convertToDto(messageRepository.save(message));
    }
}
