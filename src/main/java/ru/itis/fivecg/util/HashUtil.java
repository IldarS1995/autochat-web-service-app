package ru.itis.fivecg.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Утилитный класс для создания хешей
 */
public final class HashUtil
{
    /* Запретить создание экземпляров объектов этого класса */
    private HashUtil() { }

    /**
     * Вернуть захешированное алгоритмом MD5 значение строки
     * @param str Строка, которую нужно захешировать
     */
    public static String getMd5Hash(String str)
    {
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());

            byte[] mdbytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte mdbyte : mdbytes)
            {
                //Конвертация байтов в 16-ричные строковые знач-я
                sb.append(Integer.toString((mdbyte & 0xff) + 0x100, 16).substring(1));
            }

            return sb.toString();
        }
        catch (NoSuchAlgorithmException e)   /* Не должно выпасть */
        {
            throw new RuntimeException(e);
        }
    }
}
