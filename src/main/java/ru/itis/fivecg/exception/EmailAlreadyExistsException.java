package ru.itis.fivecg.exception;

/**
 * Исключение, которое выбрасывается, когда юзер с таким E-Mail уже присутствует в БД
 */
public class EmailAlreadyExistsException extends Exception
{
    public EmailAlreadyExistsException() { }
    public EmailAlreadyExistsException(String msg)
    {
        super(msg);
    }
}
