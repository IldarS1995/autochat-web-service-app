package ru.itis.fivecg.service;

import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.dto.UserCarDTO;

import java.util.List;

public interface CarService
{
    Long addCar(AppUser user, UserCarDTO car);
    UserCarDTO getDefaultCar(long userId);
    List<UserCarDTO> getUsersCars(long userId);

    UserCarDTO getCarByIdentification(String identification);
}
