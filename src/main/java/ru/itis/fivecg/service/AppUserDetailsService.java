package ru.itis.fivecg.service;

import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.database.domain.UserDetails;
import ru.itis.fivecg.dto.UserDetailsDTO;
import ru.itis.fivecg.dto.UserInfo;

public interface AppUserDetailsService
{
    UserDetailsDTO getUserDetails(long id);

    void addDetails(UserDetails ud);

    void addUserAccountDetails(AppUser userRet);

    UserInfo getFullUserInformation(long userId);

    void updateUserInfo(String email, UserInfo userInfo);

    void updateNetStatus(String name, String status);
}
