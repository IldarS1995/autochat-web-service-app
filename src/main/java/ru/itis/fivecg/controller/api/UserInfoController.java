package ru.itis.fivecg.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.itis.fivecg.controller.ControllerUtils;
import ru.itis.fivecg.database.domain.AppUser;
import ru.itis.fivecg.dto.*;
import ru.itis.fivecg.exception.UserNotFoundException;
import ru.itis.fivecg.service.AppUserDetailsService;
import ru.itis.fivecg.service.AppUserService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping(value = "/api/user/info")
public class UserInfoController
{
    private enum ErrorCode { FIELD_ERROR }

    @Autowired
    private AppUserDetailsService service;
    @Autowired
    private AppUserService appUserService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<InfoDTO<UserInfo>> userInfo(Principal principal)
    {
        UserAuthDataDTO user = appUserService.getUserByEmail(principal.getName());
        return userInfo(user.getId());
    }

    @RequestMapping(value = "{userId}", method = RequestMethod.GET)
    public ResponseEntity<InfoDTO<UserInfo>> userInfo(@PathVariable("userId") Long userId)
    {
        try
        {
            UserInfo userInfo = service.getFullUserInformation(userId);
            return new ResponseEntity<>(new InfoDTO<>(true, null, userInfo), HttpStatus.OK);
        }
        catch(UserNotFoundException exc)
                //User is not found in the database
        {
            List<FieldMessage> messages = Collections.singletonList(
                    new FieldMessage("id", "The user with the specified ID is not found.",
                            "USER_NOT_FOUND"));
            return new ResponseEntity<>(new InfoDTO<UserInfo>(false, messages, null),
                    HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<QueryStatus> updateUser(@RequestBody @Valid UserInfo userInfo,
                                                  BindingResult bindingResult,
                                                  Principal principal)
    {
        List<FieldMessage> messages = new ArrayList<>();
        if(bindingResult.hasFieldErrors())
        {
            //Show the user his field errors
            messages = ControllerUtils.parseFieldErrors(bindingResult.getFieldErrors(),
                    ErrorCode.FIELD_ERROR.name());
        }

        boolean success = false;
        if(messages.size() == 0)
        {
            success = true;
            String email = principal.getName();
            service.updateUserInfo(email, userInfo);
        }

        return new ResponseEntity<>(new QueryStatus(success, messages),
                success ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "updateNetStatus", method = RequestMethod.POST,
        consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<QueryStatus> updateNetStatus(@RequestBody Map<String, Object> body,
                                                       Principal principal)
    {
        List<String> statuses = Arrays.asList("ONLINE", "OFFLINE", "BUSY", "BACKGROUND");
        String status = (String)body.get("netStatus");
        boolean success = true;
        List<FieldMessage> messages = new ArrayList<>();

        if(status == null || !statuses.contains(status))
        {
            success = false;
            messages.add(new FieldMessage("netStatus", "Net status value is incorrect.",
                    "WRONG_NET_STATUS"));
        }

        if(success)
        {
            service.updateNetStatus(principal.getName(), status);
        }

        return new ResponseEntity<>(new QueryStatus(success, messages),
                success ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }
}
