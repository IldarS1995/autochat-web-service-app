package ru.itis.fivecg.exception;

/**
 * Исключение, которое выбрасывается, если введенный юзером E-Mail не удовлетворяет нашим требованиям
 */
public class EmailWrongFormatException extends RuntimeException
{
}
