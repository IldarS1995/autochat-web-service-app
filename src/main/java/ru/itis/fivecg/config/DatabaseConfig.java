package ru.itis.fivecg.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/** Spring Database configuration */
@Configuration
@PropertySource({"classpath:database.properties", "classpath:smtp.properties"})
@EnableTransactionManagement(proxyTargetClass = true)
@EnableJpaRepositories(basePackages = "ru.itis.fivecg.database.dao", entityManagerFactoryRef = "emf")
public class DatabaseConfig
{
    private static final String driverClassName = "org.postgresql.Driver";

    @Value("${db.name}")
    private String name;
    @Value("${db.host}")
    private String host;
    @Value("${db.port}")
    private String port;
    @Value("${db.username}")
    private String username;
    @Value("${db.password}")
    private String password;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer()
    {
        return new PropertySourcesPlaceholderConfigurer();
    }

    /** Data source for PostgreSQL */
    @Bean
    public DataSource dataSource()
    {
        BasicDataSource dataSource = new BasicDataSource();
        if (host.equals("OPENSHIFT_POSTGRESQL_DB_HOST"))
        {
            host = System.getenv(host);
        }
        if (port.equals("OPENSHIFT_POSTGRESQL_DB_PORT"))
        {
            port = System.getenv(port);
        }
        if (name.equals("OPENSHIFT_APP_NAME"))
        {
            name = System.getenv(name);
        }
        if(username.equals("OPENSHIFT_POSTGRESQL_DB_USERNAME"))
        {
            username = System.getenv(username);
        }
        if(password.equals("OPENSHIFT_POSTGRESQL_DB_PASSWORD"))
        {
            password = System.getenv(password);
        }

        String url = "jdbc:postgresql://"+host+":"+port+"/"+name;
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean emf()
    {
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource());
        emf.setPackagesToScan("ru.itis.fivecg.database.domain");
        emf.setPersistenceProvider(new HibernatePersistenceProvider());
        emf.setJpaProperties(jpaProperties());
        return emf;
    }

    private Properties jpaProperties()
    {
        Properties properties = new Properties();
        properties.setProperty("hibernate.show_sql", "true");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");
        properties.setProperty("hibernate.enable_lazy_load_no_trans", "true");
        properties.setProperty("hibernate.hbm2ddl.auto", "update");
        return properties;
    }

    @Bean
    public JpaTransactionManager transactionManager()
    {
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setDataSource(dataSource());
        return manager;
    }
}
