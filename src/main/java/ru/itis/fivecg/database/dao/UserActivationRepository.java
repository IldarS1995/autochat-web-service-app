package ru.itis.fivecg.database.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.fivecg.database.domain.UserActivation;

public interface UserActivationRepository extends JpaRepository<UserActivation, Long>
{
    UserActivation findByUserId(long userId);

    UserActivation findByUser_Email(String email);
}
